module ApplicationHelper
  include ActionView::Helpers::DateHelper
  def authorize_user!
    if params[:id].present? && current_user.id != params[:id]
      raise Exceptions::UnauthorizedException
    end
  end

  def requested_user
    # gets user id specified in the URL or current_user.id (as when route is accessed via /accounts)
    User.find user_id
  end

  def user_id
    params[:id] || (current_user ? current_user.id : nil)
  end

  def paginate(scope, name = nil, default_per_page = 10, url = nil, alter = nil)
    current, total, per_page = 0,1,0
    collection = []
    if alter.nil?
      collection = scope.page(params[:page]).per((params[:per_page] || default_per_page).to_i)

      current, total, per_page = collection.current_page, collection.total_pages, collection.limit_value

      collection_name = name || collection.model.name.downcase.pluralize
    else

      page = params[:page].to_i || 0
      off_set = ( page ) * 10 
      per_pick = params[:per_page].present? ? params[:per_page].to_i : 10

      collection = alter.drop(off_set).first((per_pick))

      current = params[:page].present? ? params[:page].to_i : 1
      total =  (alter.size.to_f / per_pick).ceil if (alter.size >= 10)
      per_page = per_pick.to_i
    end

    pagination = {
      pagination: {
        current:  current,
        # previous: (current > 1 ? "#{request.fullpath}?page=#{(current - 1)}" : nil),
        # next:     (current >= total ? nil : "#{request.fullpath}?page=#{(current + 1)}"),
        previous: url.nil? ? (current > 1 ? "#{request.path}?page=#{(current - 1)}" : nil) : (current > 1 ? url.call+"&page=#{(current - 1)}" : nil),
        next: url.nil? ? (current >= total ? nil : "#{request.path}?page=#{(current + 1)}") : (current >= total ? nil : url.call+"&page=#{(current + 1)}"),
        per_page: per_page,
        pages:    total,
        count:    scope.present? ? collection.total_count : alter.size
      },
      status: 200
    }

    return collection, pagination

  end

  def in_logger(obj)
    @logger = Logger.new(STDOUT)
    @logger.info("#{obj}")
  end

  def user_info(user)
    user = {
      id: user.id,
      name: user.name,
      username: user.username,
      email: user.email,
      avatar: user.avatar.present? ? user.avatar : ""
    } 
  end

  def comment_info(comment)
    comment = {
      id: comment.id,
      user: user_info(comment.user),
      message: comment.message,
      time: notif_time(comment.created_at)
    }
  end

  def notif_time(date)
    # date = date.to_i
    date = Time.at(date.to_i)

    if date.strftime("%^B %d, %Y") == DateTime.now.strftime("%^B %d, %Y")
      distance = distance_of_time_in_words(date, DateTime.now, scope: 'datetime.distance_in_words')
      distance = distance.include?("about") ? distance.split(" ").last + " ago" : distance
      return distance == "Just Now" ? distance : distance

    else
      if date.strftime('%^B %V, %Y') == DateTime.now.strftime('%^B %V, %Y')
        days =  DateTime.now.strftime('%d').to_i - date.strftime('%d').to_i
         return days.to_s + "d ago"
      else 
        week = DateTime.now.strftime('%V').to_i - date.strftime('%V').to_i
        return week.to_s + "w ago"
      end

    end
  end

  def time(date)
    if date.strftime("%^B %d, %Y") == DateTime.now.strftime("%^B %d, %Y")
      distance = distance_of_time_in_words(date, DateTime.now)
      return distance == "Just Now" ? distance : distance

    else
      
      if date.strftime("%^B %V, %Y") == DateTime.now.strftime('%^B %V, %Y')
        return date.strftime("%A")
      else 
        week = DateTime.now.strftime("%V").to_i - date.strftime("%V").to_i
        return week.to_s + "w ago"
      end
      # return date.strftime("%A, %B %d, %Y")

    end
  end

  def get_date(date)
    return "#{date.strftime('%B')} #{date.day.ordinalize}"
  end

  def get_time(date)
    return "#{date.strftime('%I:%M%P')}"
  end

end
