class Api::V1::UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_user!, only: [ :update ]

  def show
    @user = requested_user
    render 'api/v1/users/show'
  end

  def update
    current_user.update_attributes!(update_params)
    @user = current_user
    render 'api/v1/users/show'
  end

  def followers
    @followers, @pagination = paginate(requested_user.user_followers)
    render 'api/v1/users/followers'
  end

  def following
    @following, @pagination = paginate(requested_user.following_users)
    render 'api/v1/users/following'
  end

  def follow
    to_follow = User.find params[:id]
    current_user.follow(to_follow)
    render json: { message: "Successfully followed user with id '#{to_follow.id}.", status: 200 }, status: :ok
  rescue Exception => e
    render json: { error: e.message, status: 400 }, status: :bad_request
  end

  def unfollow
    to_unfollow = User.find params[:id]
    current_user.stop_following(to_unfollow)
    render json: { message: "Successfully unfollowed user with id '#{to_unfollow.id}.", status: 200 }, status: :ok
  rescue Exception => e
    render json: { error: e.message, status: 400 }, status: :bad_request
  end

  def feed
    @boasts, @pagination = paginate(requested_user.feed)
    render 'api/v1/users/feed'
  end

  private
    def update_params
      params.require(:user)
            .permit(:first_name, 
                    :last_name, 
                    :email, 
                    :account_type_id,
                    :country,
                    :postal_code,
                    :education,
                    :industry_ids => []
                    )
    end
end
