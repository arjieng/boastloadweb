class Api::V1::IndustriesController < ApplicationController

  def index
    # if index_params.include?(:last_updated_only)
    #   render json: { last_updated: Industry.maximum(:updated_at), status: 200 }, status: :ok
    #   return
    # end

    industries = Industry.all

    render json: { industries: industries.as_json, status: 200 }, status: :ok

  end

end