class Api::V1::AccountTypesController < ApplicationController

  def index
    account_types = AccountType.where.not(user_type: 'unknown')

    render json: { account_types: account_types.as_json, status: 200 }, status: :ok
  end

end
