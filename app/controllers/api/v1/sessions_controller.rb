class Api::V1::SessionsController < DeviseTokenAuth::SessionsController
    def create
    # super
    user = User.find_by(username: params[:username])
    # user = User.find_by_email params[:username] if user.nil?
    
    if user.nil? || !user.valid_password?(params[:password])
      render json: { "error" => "Invalid Username / Password.", status: 404 }, status: 400
    else
      client_id = SecureRandom.urlsafe_base64(nil, false)
      token     = SecureRandom.urlsafe_base64(nil, false)

      # store client + token in user's token hash
      user.tokens[client_id] = {
        token: BCrypt::Password.create(token),
        expiry: (Time.now + user.token_lifespan).to_i
      }

      # generate auth headers for response
      new_auth_header = user.build_auth_header(token, client_id)

      # update response with the header that will be required by the next request
      response.headers.merge!(new_auth_header)

      user.save!

      render json: { data: user.as_json, status: 200 }, status: 200
    end
  end
end
