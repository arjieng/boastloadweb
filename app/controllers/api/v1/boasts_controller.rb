class Api::V1::BoastsController < ApplicationController
  before_action :authenticate_user!, only: [ :index, :create, :destroy, :rank ]
  before_action :authorize_user!, only: [ :create, :destroy ]

  def index
    @boasts, @pagination = paginate(requested_user.boasts)
    render 'api/v1/boasts/index'
  end

  def create
    unless create_params.present?
      raise ActionController::ParameterMissing "videos"
    end

    videos = create_params

    # check duration
    video_duration = 0
    durations = videos.map do |video|
      file = FFMPEG::Movie.new(video.tempfile.path)
      file.duration
    end

    video_duration = durations.sum

    if (video_duration - 60) > 1
      render json: { error: "Uploaded videos exceeded 1 minute.", status: 400 }, status: :bad_request
      return
    elsif (video_duration - 60) < -1
      render json: { error: "The total length of uploaded videos was less than 1 minute.", status: 400 }, status: :bad_request
      return
    end

    @boast = current_user.boasts.create
    videos.map do |video|
      @boast.videos.attach(
        io: File.open(video.tempfile.path), 
        filename: video.original_filename,
        content_type: video.content_type
      )
    end

    @boast.process videos

    if @boast.save!
      render json: { success: "Boast successfully uploaded.", boast: @boast, status: 200 }, status: :ok
    end

  end

  def rank
    boast = Boast.find params[:id]
    @boast = current_user.rank boast, rank_params[:rating]
    render json: { success: "Boast successfully ranked.", boast: @boast, status: 200 }, status: :ok
  end

  def tags
    unless keyword_params.present?
      raise ActionController::ParameterMissing "keywords"
    end

    boast = Boast.find params[:boast_id]
    keywords = params[:keywords]
    
    if boast.tag_list.present?
      keywords.each do |keyword|
        (boast.tag_list.include? keyword) unless boast.tag_list.add(keyword)
      end
    else
      boast.tag_list = keywords
    end
    
    if boast.save!
      render json: { success: "Successfully added tags to boast", status: 200 }, status: 200    
    end
  end

  def remove_tags
    unless keyword_params.present?
      raise ActionController::ParameterMissing "keywords"
    end

    boast = Boast.find params[:boast_id]
    keywords = params[:keywords]

    if boast.tag_list.present?
      keywords.each do |keyword|
        (boast.tag_list.include? keyword) if boast.tag_list.remove(keyword)
      end
    end

    if boast.save!
      render json: { success: "Successfully removed tags from boast", status: 200 }, status: 200
    end
  end

  def search_by_tags
    unless keyword_params.present?
      raise ActionController::ParameterMissing "keywords"
    end

    boasts = Boast.tagged_with(params[:keywords], any: :true)
    @boasts, @pagination = paginate(boasts)
    render 'api/v1/boasts/index'
  end

  def mention
    unless mention_params.present?
      raise ActionController::ParameterMissing "user"
    end

    boast = Boast.find params[:boast_id]
    users = params[:users]

    users.each do |user|
      boast.mention(User.find user)
      #Trigger push notif
    end

    if boast.save!
      render json: { success: "Successfully added mentionees to boast", status: 200 }, status: 200
    end
  end

  def unmention
    unless mention_params.present?
      raise ActionController::ParameterMissing "user"
    end

    boast = Boast.find params[:boast_id]
    users = params[:users]

    users.each do |user|
      @user = User.find user
      boast.mentions? @user if boast.unmention @user
    end

    if boast.save!
      render json: { success: "Successfully removed mentionees from boast", status: 200 }, status: 200
    end
  end

  private
    def create_params
      params.require(:videos)
    end

    def rank_params
      params.permit(:rating)
    end

    def keyword_params
      params.require(:keywords)
    end

    def mention_params
      params.require(:users)
    end

    def page
      params[:page] || 0
    end

    

end
