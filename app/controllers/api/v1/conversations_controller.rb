class Api::V1::ConversationsController < ApplicationController
 before_action :authenticate_user!, only: [ :index, :create, :destroy]
 before_action :authorize_user!, only: [ :create, :destroy ]


  def index
    if params[:conversation_id].present?
      conversation = current_user.mailbox.conversations.includes(:receipts).find params[:conversation_id] 
      conversation.mark_as_read(current_user)
      receipts = conversation.receipts_for(current_user).includes(notification: :sender).where("trashed = false and deleted = false").order("created_at desc")
      url = Proc.new { api_v1_user_conversations_path(conversation_id: params[:conversation_id]) }
      @receipts, @pagination = paginate(receipts, nil, 10, url)
      render 'api/v1/conversations/index'
    else
      render json: { notice: "No active conversation", status: 201 }, status: 201
    end
  end

  def create
  	unless message_params.present?
      raise ActionController::ParameterMissing "message"
    end

    receiver = User.find params[:user_id]
   	Mailboxer::Conversation.transaction do
   	  if !params[:conversation_id].present?
    
        receipt = current_user.send_message(receiver, params[:message][:text], "#{current_user.first_name}-#{receiver.first_name}")
        receipt.update_attribute(:is_read, false)
        #push notif or web socket to send message dynamically
        render json: { success: "Message successfully sent", conversation_id: receipt.conversation.id, status: 200 }, status: 200
      else
        conversation = current_user.mailbox.conversations.find_by(id: params[:conversation_id])
        receipt = current_user.reply_to_conversation(conversation, params[:message][:text])
        receipt.update_attribute(:is_read, false)
 	    #push notif or web socket to send message dynamically
        render json: { success: "Message successfully sent", conversation_id: receipt.conversation.id, status: 200 }, status: 200
      end
   	end
  end

  private
  def message_params
  	params.require(:message).permit(:text)
  end

  def page
    params[:page] || 0
  end
   

end