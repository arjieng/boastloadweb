class Api::V1::CommentsController < ApplicationController
  before_action :authenticate_user!, only: [:index, :create, :destroy]
  before_action :authorize_user!, only: [:index, :create, :destroy ]

  def index
    comments = Comment.includes(:replies).where("commentable_id = ? and commentable_type = 'Boast'", params[:boast_id]).order("created_at asc")
    @comments, @pagination = paginate(comments)
    render 'api/v1/comments/index'
  end
  
  def create
  	unless create_params.present?
      raise ActionController::ParameterMissing "comment"
    end
  
    comment = Comment.create(create_params)
    comment.commentable_id = params[:boast_id]
    comment.user_id = current_user.id

    if comment.save!
	    render json: { success: "Comment successfully posted.", comment: comment, status: 200 }, status: :ok
    end
  end

  private
    def create_params
      params.require(:comment).permit(:message, :commentable_type, :comment_id)
    end

end