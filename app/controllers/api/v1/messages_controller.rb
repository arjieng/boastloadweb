class Api::V1::MessagesController < ApplicationController
  before_action :authenticate_user!, only: [ :index, :create, :destroy]
  before_action :authorize_user!, only: [ :create, :destroy ]


  def index
    conversations = current_user.mailbox.conversations.includes(receipts: :receiver)
	@conversations = []
	conversations.each do |conversation|
	  receipt = conversation.receipts_for(current_user).last
	  if !receipt.deleted
	    @conversations.push(conversations_info(conversation))
	  end
	end
	url = Proc.new{ api_v1_users_messages_path }
    @convos, @pagination = paginate(nil,nil,0,nil, @conversations)
    render 'api/v1/messages/index'
  end

  def destroy
  	
  end

private
  def conversations_info(conversation)
	sender = conversation.receipts.where("receiver_id != ?", current_user.id).last.receiver
	receipt = conversation.receipts_for(current_user).last
	notification = conversation.last_message
	new_messages = conversation.receipts_for(current_user).where("is_read = false").size
	  
	is_owner = notification.sender_id == current_user.id ? 1 : 0

	message = { user: { id: sender.id, first_name: sender.first_name, image: sender.avatar }, message: notification.body, time: time(notification.created_at), new_messages: new_messages }

	return { id: conversation.id, is_read: is_owner == 1 ? 1 : (receipt.is_read? ? 1 : 0), notification: message }
	 
  end
end