json.id boast.id
json.video_url boast.video_url
json.thumbnail boast.thumbnail_url
json.caption boast.caption
json.score boast.score
json.your_rating current_user.ranking_of(boast)

if current_user != boast.user
  json.owner boast.user, partial: 'partials/user_lite', as: :user
end