json.id user.id
json.name user.name
json.username user.username
json.account_type user.account_type.name
json.avatar user.avatar
json.following user.follow_count
json.followers user.followers_count

if current_user == user
  json.email user.email
end