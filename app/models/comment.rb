class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :commentable, polymorphic: true
  has_many :replies, class_name: "Comment"

  act_as_mentioner

private
  # def as_json(options={})
  #   super( except: [ :created_at, :updated_at, :commentable_id, :commentable_type ] )
  # end
end 
