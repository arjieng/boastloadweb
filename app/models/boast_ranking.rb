class BoastRanking < ApplicationRecord
  belongs_to :boast
  belongs_to :user

  validates :rating, presence: true, numericality: { only_integer: true, greater_than: 0, less_than_or_equal_to: 5 }
  validates :boast, uniqueness: { scope: :user, message: "You have already ranked this boast." }
end
