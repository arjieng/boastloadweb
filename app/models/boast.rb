class Boast < ApplicationRecord
  include ApplicationHelper
  belongs_to :user
  has_one_attached :boast_video  # the compiled boast video
  has_many_attached :videos      # the 10-second videos, if applicable
  has_one_attached :thumbnail
  # has_many :boast_rankings, as: "rankings"
  has_many :rankings, foreign_key: "boast_id", class_name: "BoastRanking"
  has_many :comments, as: :commentable

  acts_as_taggable
  acts_as_taggable_on :users
  act_as_mentioner


  # @@transcode_options = {
  #   video_codec: "libx264", 
  #   frame_rate: 29.96, 
  #   resolution: "600x600", 
  #   video_bitrate: 300, 
  #   video_bitrate_tolerance: 100,
  #   keyframe_interval: 90, 
  #   x264_vprofile: "high", 
  #   x264_preset: "slow",
  #   audio_codec: "libfaac", 
  #   audio_bitrate: 32, 
  #   audio_sample_rate: 22050, 
  #   audio_channels: 1,
  #   threads: 2
  # }

  before_save :update_video_url

  def process videos
    # concatenate videos
    movie = FFMPEG::Movie.new(videos[0].tempfile.path)
    input_file = Tempfile.new('input.txt')
    output_file_path = "/tmp/boast-#{id}.mp4"

    videos.each do |video|
      input_file.write "file '#{video.tempfile.path}'\n"
    end
    input_file.close

    transcoder_opts = { custom: %w(-c copy) }
    input_opts = { input: input_file.path, input_options: { f: 'concat', safe: '0' } } 

    transcoded = movie.transcode(output_file_path, transcoder_opts, input_opts)
    boast_video.attach(io: File.open(output_file_path), filename: 'boast-#{id}.mp4', content_type: 'video/mp4')

    # generate thumbnail
    thumbnail_path = "/tmp/boast-#{id}_thumb.png"
    screenshot = transcoded.screenshot(thumbnail_path, seek_time: 30)
    thumbnail.attach(io: File.open(thumbnail_path), filename: 'boast-#{id}_thumb.png', content_type: 'image/png')    
  rescue Exception => e
    logger.error "Error transcoding video: #{e.message}"

  ensure
    input_file.unlink if input_file != nil
  end

  def update_video_url
    if boast_video.attached?
      self.video_url = Rails.application.routes.url_helpers.rails_blob_path(boast_video, only_path: true)
    end

    if thumbnail.attached?
      self.thumbnail_url = Rails.application.routes.url_helpers.rails_blob_path(thumbnail, only_path: true)
    end
  end

  def score
    rankings.sum(:rating) || 0
  end

  def tags
    self.tag_list
  end

  def mentionees
    mentionees = self.mentionees_by(User).map(&:mentionee)
    @mentionees = []
    mentionees.each do |mentionee|
      @mentionees.push(user_info(mentionee))
    end

    return @mentionees
  end

  def weekly_score
    0
  end

  def monthly_score
    0
  end

  def as_json(options={})
    super( except: [ :created_at, :updated_at, :user_id, :latitude, :longitude, :full_address, :place_id ] )
  end

end
