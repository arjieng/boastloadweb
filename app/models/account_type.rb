class AccountType < ApplicationRecord
  has_many :users

  enum user_type: {
    unknown: 0,
    jobseeker: 1,
    employer: 2
  }

  def self.change_allowed? type1, type2
    type1 = AccountType.find type1 if type1.is_a? Integer
    type2 = AccountType.find type2 if type2.is_a? Integer

    type1.user_type == type2.user_type
  end

  def as_json options={}
    super({except: [ :created_at, :updated_at, :needs_payment, :user_type ]})
  end

end
