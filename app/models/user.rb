class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  # associations
  has_many :boasts
  has_many :user_industries, dependent: :destroy
  has_many :industries, :through => :user_industries, :source => :industry
  has_many :comments, dependent: :destroy
  belongs_to :account_type

  acts_as_followable
  acts_as_follower
  act_as_mentionee
  acts_as_messageable

  # validations
  validates :industries, length: { maximum: 3, message: 'cannot have more than 3' }
  validates :username, presence: true, uniqueness: true
  validates :email, presence: true, uniqueness: true
  validate :account_type_change_permitted?
  # validates :country, inclusion: { in: ISO3166::Country.all.map(&:alpha2) }

  def profile_complete?
    !(account_type.user_type == 'unknown')
    # || account_type.needs_payment && 
  end

  def name
    first_name + " " + last_name
  end

  def rank boast, rating
    owner = boast.user
    if self.following? owner # || boast.in_challenge
      return boast.rankings.create!(user: self, rating: rating)
    else
      raise Exceptions::UnauthorizedException.new("You cannot rank this boast.")
    end
  end

  def ranking_of boast
    boast.rankings.where(user_id: self.id).sum(:rating)
  end

  def feed
    _following_ids = self.following_users.pluck(:id)
    boasts = Boast.where(user_id: _following_ids).order(created_at: :desc)
  end

  def mentioners
    self.mentioners_by(Boast)
  end

  def as_json options={}
    super({
      except: [ :created_at, :updated_at, :allow_password_change, :provider, :account_type_id, :uid ],
      include: [ :account_type, :industries ]
    })
  end

  private
    def account_type_change_permitted?
      if account_type_id_changed? && self.persisted?
        prev_account_type = AccountType.find account_type_id_was
        if prev_account_type.user_type != 'unknown' && !AccountType.change_allowed?(account_type_id_was, account_type_id)
          errors.add(:account_type, "Change of account type is not allowed!")
        end
      end
    end
end
