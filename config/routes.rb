Rails.application.routes.draw do

  scope :api do
    scope :v1 do
      mount_devise_token_auth_for 'User', at: 'auth', controllers: {
        token_validations:  'api/v1/sessions',
        sessions: 'api/v1/sessions',
        registrations: 'api/v1/registrations'
      }
    end
  end

  namespace :api, defaults: { format: :json } do
    namespace :v1 do

      resources :industries, only: [ :index ]
      resources :account_types, only: [ :index ]
    
      resources :users, except: [ :create, :new, :edit, :destroy, :update ], defaults: { format: :json} do
        resources :conversations, only: [:index, :create]
        member do
          resources :boasts, only: [ :index ] 
          get :followers, to: 'users#followers'
          get :following, to: 'users#following'
          post :follow, to: 'users#follow'
          post :unfollow, to: 'users#unfollow'
          get :feed, to: 'users#feed'
        end
      end

      resource :users, path: :account, except: [ :new, :edit ]  do
        resources :boasts, only: [ :create, :index, :destroy ] do
          post :tags
          post :mention
          delete :unmention
          delete 'remove-tags', to: 'boasts#remove_tags'
        end
        resources :messages, only: [:index, :destroy]
        get :followers, to: 'users#followers'
        get :following, to: 'users#following'
        get :feed, to: 'users#feed'
      end

      resources :boasts, only: [ :rank ] do 
        resources :comments, only: [:index, :create]
        member do
          post :rank
        end
        collection{
          post 'search-by-tags' => "boasts#search_by_tags"

        }
      end
    
    end #end v1 namespace

  end  # end api namespace

end

# https://stackoverflow.com/questions/9627546/api-versioning-for-rails-routes?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa