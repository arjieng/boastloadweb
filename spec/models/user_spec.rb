require 'rails_helper'

RSpec.describe User, type: :model do

  context "validation" do
    it "should allow users to switch account types if intially not set" do
      user = FactoryBot.create :user
      other_account_type = FactoryBot.create :account_type, :employer
      user.account_type_id = other_account_type.id
      user.save
      expect(user.reload.account_type_id).to eq(other_account_type.id) 
    end

    it "should not allow jobseekers to switch to employer" do
      user = FactoryBot.create :user, :jobseeker
      other_account_type = FactoryBot.create :account_type, :employer
      user.account_type_id = other_account_type.id
      user.save
      expect(user.errors[:account_type]).to  eq(["Change of account type is not allowed!"])
      expect(user.reload.account_type_id).not_to eq(other_account_type.id) 
    end

    it "should not allow employers to switch to jobseeker" do
      user = FactoryBot.create :user, :employer
      other_account_type = FactoryBot.create :account_type, :jobseeker
      user.account_type_id = other_account_type.id
      user.save
      expect(user.errors[:account_type]).to  eq(["Change of account type is not allowed!"])
      expect(user.reload.account_type_id).not_to eq(other_account_type.id) 
    end

    it "should be successful if industries up to 3" do
      user = FactoryBot.build :user
      user.industries << FactoryBot.create_list(:industry, 3)
      user.save
      expect(User.count).to eq(1)
    end

    it "should raise an error if more than 3 industries" do
      user = FactoryBot.build :user
      user.industries << FactoryBot.create_list(:industry, 4)
      user.save
      expect(user.errors[:industries]).to eq(['cannot have more than 3'])
    end
  end

  context "ranking boasts" do
    before(:each) do
      @user = FactoryBot.create :user
    end

    it "should allow the user to rank a boast of a user they follow" do
      other_user = FactoryBot.create :user
      boast = FactoryBot.create(:boast, user_id: other_user.id)

      @user.follow(other_user)
      @user.rank boast, 1

      expect(BoastRanking.count).to eq(1)
    end

    it "should not allow the user to rank a boast of a user they do not follow" do
      other_user = FactoryBot.create :user
      boast = FactoryBot.create(:boast, user_id: other_user.id)

      expect { @user.rank(boast, 1) }.to raise_exception
      expect(BoastRanking.count).to eq(0)
    end

    it "should not allow the user to rank same boast more than once" do
      other_user = FactoryBot.create :user
      boast = FactoryBot.create(:boast, user_id: other_user.id)
      @user.follow(other_user)
      @user.rank boast, 1

      expect(BoastRanking.count).to eq(1)

      expect { @user.rank(boast, 1) }.to raise_exception
      expect(BoastRanking.count).to eq(1)
    end

    it "should allow the user to rank a boast on a challenge" do
    end
  end
end
