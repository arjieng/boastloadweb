def add_followers(user)
  followers = FactoryBot.create_list(:user, 10)
  followers.each do |follower|
    follower.follow(user)
  end
  return followers
end

def add_followings(user)
  followings = FactoryBot.create_list(:user, 10)
  followings.each do |following|
    user.follow(following)
  end
  return followings
end
