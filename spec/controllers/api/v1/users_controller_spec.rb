require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  render_views

  before(:each) do
    @user = FactoryBot.create :user
    auth_headers = token_sign_in(@user)
    request.headers.merge!(auth_headers)
  end

  describe "GET #followers" do
    before(:each) do
      @followers = add_followers(@user)
    end

    it "should return the user's followers" do
      get :followers, params: { user_id: @user.id, format: :json }

      body = JSON.parse response.body, symbolize_names: true
      ids = body[:followers].map{ |e| e[:id] }
      names = body[:followers].map{ |e| e[:name] }

      expect(response.status).to eq(200)
      expect(ids).to include *@followers.map(&:id)
      expect(names).to include *@followers.map(&:name)
    end
  end

  describe "GET #followings" do
    before(:each) do
      @followings = add_followings(@user)
    end

    it "should return the users that the user is following" do
      get :following, params: { user_id: @user.id, format: :json }

      body = JSON.parse response.body, symbolize_names: true
      ids = body[:following].map{ |e| e[:id] }
      names = body[:following].map{ |e| e[:name] }

      expect(response.status).to eq(200)
      expect(ids).to include *@followings.map(&:id)
      expect(names).to include *@followings.map(&:name)
    end
  end

  describe "POST #follow" do
    context "user logged in" do
      it "should let the user follow another" do
        other_user = FactoryBot.create :user
        post :follow, params: { id: other_user.id, format: :json }

        expect(response.status).to eq(200)
        expect(@user.follow_count).to eq(1)
      end

      it "should not let the user follow himself" do
        post :follow, params: { id: @user.id, format: :json }

        expect(response.status).to eq(200)
        expect(@user.follow_count).to eq(0)
      end
    end

    context "user not logged in" do
      it "should respond with Unauthorized" do
        
        request.headers['access-token'] = ""
        request.headers['client'] = ""
        request.headers['uid'] = ""

        other_user = FactoryBot.create :user
        post :follow, params: { id: other_user.id, format: :json }

        expect(response.status).to eq 401
      end
    end
  end

  describe "POST #unfollow" do
    context "user logged in" do
      it "should let the user unfollow another" do
        other_user = FactoryBot.create :user
        @user.follow(other_user)
        post :unfollow, params: { id: other_user.id, format: :json }

        expect(response.status).to eq(200)
        expect(@user.follow_count).to eq(0)
      end
    end

    context "user not logged in" do
      it "should respond with Unauthorized" do
        request.headers['access-token'] = ""
        request.headers['client'] = ""
        request.headers['uid'] = ""

        other_user = FactoryBot.create :user
        post :follow, params: { id: other_user.id, format: :json }

        expect(response.status).to eq 401
      end
    end

  end

  describe "GET #feed" do
    before(:each) do
      follows = add_followings(@user)
      @boasts = []
      follows.each do |f|
        # 3.times do |t|
          @boasts << FactoryBot.create(:boast, user_id: f.id)
        # end
      end
    end

    context "user logged in" do
      it "should fetch the user's feed" do
        get :feed, params: { id: @user.id, format: :json }

        body = JSON.parse response.body, symbolize_names: true
        ids = body[:boasts].map{ |e| e[:id] }
        video_urls = body[:boasts].map{ |e| e[:video_url] }

        expect(response.status).to eq(200)
        expect(ids).to include *@boasts.map(&:id)
        expect(video_urls).to include *@boasts.map(&:video_url)
      end
    end

    context "user not logged in" do
      it "should respond with Unauthorized" do
        
        request.headers['access-token'] = ""
        request.headers['client'] = ""
        request.headers['uid'] = ""

        get :feed

        expect(response.status).to eq 401
      end
    end
  end

end
