require 'rails_helper'

RSpec.describe Api::V1::BoastsController, type: :controller do
  render_views 

  before(:each) do
    @user = FactoryBot.create :user
    auth_headers = token_sign_in(@user)
    request.headers.merge!(auth_headers)
  end
  
  describe "GET #index" do
    before(:each) do
      10.times do |t|
        FactoryBot.create(:boast, user_id: @user.id)
      end
      @boasts = @user.boasts
    end

    it "should return a list of boasts" do
      get :index, params: { user_id: @user.id, format: :json }

      body = JSON.parse response.body, symbolize_names: true
      ids = body[:boasts].map{ |e| e[:id] }
      video_urls = body[:boasts].map{ |e| e[:video_url] }

      expect(response.status).to eq(200)
      expect(ids).to include *@boasts.map(&:id)
      expect(video_urls).to include *@boasts.map(&:video_url)
    end

    context "user not logged in" do
      it "should respond with Unauthorized" do
        
        request.headers['access-token'] = ""
        request.headers['client'] = ""
        request.headers['uid'] = ""

        get :index, params: { user_id: @user.id, format: :json }

        expect(response.status).to eq 401
      end
    end
  end

  describe "POST #create" do
    context "uploading six 10-second videos" do
      it "should be successful" do
        path = Rails.root.join('spec','fixtures', 'files', '10.mp4')
        params = { 
          "videos" => [
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true)
         ] 
        }

        post :create, params: params

        expect(Boast.count).to eq(1)
      end
    end

    context "uploading one 60-second video" do
      it "should be successful" do
        path = Rails.root.join('spec','fixtures', 'files', '60.mp4')
        params = { 
          "videos" => [
             Rack::Test::UploadedFile.new(path, 'video/mp4', true)
          ] 
        }

        post :create, params: params

        expect(Boast.count).to eq(1)
      end
    end

    context "uploading videos whose total duration is under 60 seconds" do
      it "should give an error" do
        path = Rails.root.join('spec','fixtures', 'files', '10.mp4')
        params = { 
          "videos" => [
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
         ] 
        }

        post :create, params: params

        expect(response.status).to eq 400
        body = JSON.parse response.body, symbolize_names: true
        expect(body[:error]).to eq "The total length of uploaded videos was less than 1 minute."
      end
    end

    context "uploading videos whose total duration exceeds 60 seconds" do
      it "should give an error" do
        path = Rails.root.join('spec','fixtures', 'files', '11.mp4')
        params = { 
          "videos" => [
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
             Rack::Test::UploadedFile.new(path, 'video/mp4', true), 
         ] 
        }

        post :create, params: params

        expect(response.status).to eq 400
        body = JSON.parse response.body, symbolize_names: true
        expect(body[:error]).to eq "Uploaded videos exceeded 1 minute."
      end
    end


    context "user not logged in" do
      it "should respond with Unauthorized" do
        path = Rails.root.join('spec','fixtures', 'files', '60.mp4')
        params = { 
          "videos" => [
             Rack::Test::UploadedFile.new(path, 'video/mp4', true)
         ] 
        }

        request.headers['access-token'] = ""
        request.headers['client'] = ""
        request.headers['uid'] = ""

        post :create, params: params

        expect(response.status).to eq 401
      end
    end

  end
end
