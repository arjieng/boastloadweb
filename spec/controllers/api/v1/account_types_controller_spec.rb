require 'rails_helper'

RSpec.describe Api::V1::AccountTypesController, type: :controller do
  describe "GET #index" do
    it "should return a list of account types" do
      @account_types = (1..3).map{ |x|
        AccountType.create!(name: "Account Type #{x}", user_type: 'jobseeker')
      }

      get :index

      body = JSON.parse response.body, symbolize_names: true
      ids = body[:account_types].map{ |e| e[:id] }
      names = body[:account_types].map{ |e| e[:name] }

      expect(response.status).to eq(200)
      expect(ids).to include *@account_types.map(&:id)
      expect(names).to include *@account_types.map(&:name)
    end
  end
end
