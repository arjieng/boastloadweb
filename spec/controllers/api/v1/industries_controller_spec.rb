require 'rails_helper'

RSpec.describe Api::V1::IndustriesController, type: :controller do
  describe "GET #index" do
    it "should return a list of industries" do
      @industries = FactoryBot.create_list(:industry, 3)
      get :index

      body = JSON.parse response.body, symbolize_names: true
      ids = body[:industries].map{ |e| e[:id] }
      names = body[:industries].map{ |e| e[:name] }

      expect(response.status).to eq(200)
      expect(ids).to include *@industries.map(&:id)
      expect(names).to include *@industries.map(&:name)
    end
  end

end