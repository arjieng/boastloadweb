FactoryBot.define do
  factory :boast do
    caption "Hello world!"
    after(:build) do |b|
      b.boast_video.attach(io: File.open(Rails.root.join('spec', 'fixtures', 'files', '60.mp4')), filename: 'boast.mp4', content_type: 'video/mp4')
      b.thumbnail.attach(io: File.open(Rails.root.join('spec', 'fixtures', 'files', 'thumb.jpg')), filename: 'thumb.jpg', content_type: 'image/jpeg')
    end
  end
end
