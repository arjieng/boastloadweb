FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "user#{n}@boastload.com"}
    password "password"
    password_confirmation "password"
    first_name 'Annie'
    last_name 'Kroy'
    sequence(:username) { |n| "user_#{n}"}
    new_account
    
    trait :jobseeker do
      account_type { FactoryBot.create(:account_type, :jobseeker) }
    end

    trait :new_account do
      account_type { FactoryBot.create(:account_type, :unknown) }
    end

    trait :employer do
      account_type { FactoryBot.create(:account_type, :employer) }
    end

  end

end
