FactoryBot.define do
  factory :account_type  do
    sequence(:name) { |n| "Account Type #{n}"}

    trait :unknown do
      user_type 'unknown'
    end

    trait :jobseeker do
      user_type 'jobseeker'
    end

    trait :employer do
      user_type 'employer'
    end

    trait :free do
      needs_payment false
    end

    trait :paid do
      needs_payment true
    end
  end


end