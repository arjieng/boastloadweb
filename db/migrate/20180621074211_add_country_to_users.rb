class AddCountryToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :country, :string, default: ''
    add_column :users, :country_code, :string, default: ''
    add_column :users, :postal_code, :string, default: ''
  end
end
