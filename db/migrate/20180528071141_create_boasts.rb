class CreateBoasts < ActiveRecord::Migration[5.2]
  def change
    create_table :boasts do |t|
      t.string :video_url
      t.string :thumbnail_url
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
