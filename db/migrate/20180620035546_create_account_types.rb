class CreateAccountTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :account_types do |t|
      t.string :name
      t.integer :user_type
      t.boolean :needs_payment

      t.timestamps
    end

    add_column :users, :account_type_id, :integer, default: 1
  end
end
