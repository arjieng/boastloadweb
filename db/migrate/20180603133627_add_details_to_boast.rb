class AddDetailsToBoast < ActiveRecord::Migration[5.2]
  def change
    add_column :boasts, :video_aspect_ratio, :integer, default: 0
    add_column :boasts, :latitude, :float, default: 0.0
    add_column :boasts, :longitude, :float, default: 0.0
    add_column :boasts, :full_address, :string
    add_column :boasts, :place_id, :string
    add_column :boasts, :caption, :string
  end
end
