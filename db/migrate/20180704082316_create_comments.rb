class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.belongs_to :user
      t.text :message
      t.integer :commentable_id
      t.string :commentable_type
      t.integer :comment_id
      t.timestamps
    end
  end
end
