class CreateBoastRankings < ActiveRecord::Migration[5.2]
  def change
    create_table :boast_rankings do |t|
      t.references :boast, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :rating

      t.timestamps
    end
  end
end
