class ChangeDefaultProviderInUsers < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:users, :provider, 'username')
  end
end
